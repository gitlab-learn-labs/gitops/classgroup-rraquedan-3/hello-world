FROM python:3.6-slim-bullseye

WORKDIR /app

COPY ./src .

EXPOSE 5000

CMD [ "python", "microwebserver.py" ]
